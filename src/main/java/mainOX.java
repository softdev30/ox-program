
import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class mainOX {

    Scanner kb = new Scanner(System.in);
    char board[][] = new char[3][3];
    char Player;
    private int rows = 3;
    private int columns = 3;

    public mainOX() {
        board = new char[rows][columns];           
        Player = 'o';            
        initializeBoard();
    }
    
    public void initializeBoard() {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                board[i][j] = '-';
            }
        }
    }

    public void printBoard() {
        for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 3; j++) {
                System.out.print(board[i][j]+" ");
            }
            System.out.println("");
        }
    }
    public boolean isBoardFull() {
        for(int i = 0; i < rows; i++) {
            for(int j = 0; j < columns; j++) {
                if (board[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }
    
    public boolean checkForWinner() {
        return (checkRows() || checkColumns() || checkDiagonals());
    }
    
    
    private boolean checkRows() {
        for (int i = 0; i < rows; i++) {
            if (board[i][0] != '-' && board[i][0] == board[i][1] && 
                board[i][0] == board[i][2]) {
                return true;
            }
        }
        return false;
    }
    private boolean checkColumns() {
        for (int i = 0; i < columns; i++) {
            if (board[0][i] != '-' && board[0][i] == board[1][i] && 
                board[0][i] == board[2][i]) {
                return true;
            }
        }
        return false;
    }
    private boolean checkDiagonals() {
        return (board[0][0] != '-' && board[0][0] == board[1][1] 
                && board[0][0] == board[2][2]) 
                ||
               (board[0][2] != '-' && board[0][2] == board[1][1] 
                && board[0][2] == board[2][0]);
                
    }
    public void changePlayer() {
        Player = (Player == 'o') ? 'x' : 'o';
    }

    public void updateBoard() {
        boolean validInput = false;
        do {
            System.out.println("Turn " + Player);
            System.out.println("Please input row, col:");
            int row = kb.nextInt() - 1;
            int col = kb.nextInt() - 1;

            if (row >= 0 && row < rows && col >= 0 && col < columns
                    && board[row][col] == '-') {
                board[row][col] = Player;
                validInput = true;
            } else {
                System.out.println("Sorry, the move at ("
                        + (row + 1) + ", " + (col + 1)
                        + ") is not valid. Please try again");
            }
        } while (!validInput);
    }

    public static void main(String[] args) {
        mainOX t = new mainOX();
        boolean gameStatus = false;
        System.out.println("Welcome to OX Game");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print("- ");
            }
            System.out.println("");
        }

        
        //System.out.println("Turn " + t.Player);
        //System.out.println("Please input row, col:");
        
        while (gameStatus == false){
            t.updateBoard();
            t.printBoard();
        if (t.checkForWinner()) {
                System.out.println(">>>"+t.Player+" Win<<<");
                gameStatus = true;
        }
        else if (t.isBoardFull()) {
                System.out.println(">>>DRAW<<<");
                gameStatus = true;
        }
        if(gameStatus == false){
                t.changePlayer();  
            }
        
 
    }
    }
}
